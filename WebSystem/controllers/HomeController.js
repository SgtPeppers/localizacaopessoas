
module.exports.controller = (app) => {
    
    app.get(['/api/getcameras'], (req, res) => {
        
        var db = req.app.get('db');
        var ref = db.ref("cameras");
        
        ref.once("value", function(snapshot) {
            res.json(snapshot.val());
        }, function (errorObject) {
            console.log("The read failed: " + errorObject.code);
        });

    });

    app.post("/api/addcamera", (req, res) => {

        var db = req.app.get('db');

        var refKey = db.ref("cameras").push({
            setor : req.body.setor,
            operando : req.body.operando,
            IpAdress : req.body.ip
        }).key;
        
        res.send({Key : refKey});
    });

    app.post(["/api/getcamera"], function(req, res){

        var db = req.app.get('db');

        var ref = db.ref("cameras/" + req.body.key);

        ref.once("value", function(snapshot){
            res.json(snapshot.val());
        }, function(errorObject){
            console.log("The read failed: " + errorObject.code);
        });
    });

    app.post(["/api/addcounter"], function(req, res){

        var dateFormat = req.body.year + "/" + req.body.month + "/" + req.body.day;
        stringFormat = "cameras/" + req.body.key + "/NPersons/" + dateFormat;

        var db = req.app.get('db');

        var ref = db.ref(stringFormat + "/" + req.body.hour + "H").once('value', function(snapshot){
            if(parseInt(snapshot.val()) > 0)
            {   
                counter = (parseInt(snapshot.val()) + parseInt(req.body.counter))/2;
                db.ref(stringFormat).update({
                    [req.body.hour + "H"]: counter,
                });
            }
            else
            {
                db.ref(stringFormat).update({
                    [req.body.hour + "H"]: req.body.counter
                });                
            }
        });

        res.send("ok");
    });
}