const firebase = require('firebase/app');

require('firebase/database');

const config = {
  apiKey: 'AIzaSyAklq05E8iBxIVRMjFdowJNkwNlGK5p8b4',
  authDomain: 'embarcados-94613.firebaseapp.com',
  databaseURL: 'https://embarcados-94613.firebaseio.com',
  projectId: 'embarcados-94613',
  storageBucket: 'embarcados-94613.appspot.com',
  messagingSenderId: '16247460874',
};

firebase.initializeApp(config);

const db = firebase.database();

// const starCountRef = db.ref('/cameras');
// starCountRef.on('value', function (snapshot) {
//   // eslint-disable-next-line
//   console.log(snapshot.val());
// });

// const settings = {
//     timestampsInSnapshots: true
// }

// db.settings(settings)

export {
  db,
};

