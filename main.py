#!/usr/bin/python3

import socket
import os
from urllib import request, parse
import json
import datetime
import random


listKeys = ["-LabvTn-w9QTNwV2uNQ5", "-Lac0t39Oa4FLAomIQ2L", "-Lap-1B70H_oKCH14ksR", "-LaqJ1dXSZhXhokSGAMQ", "-LbK7egdxMZNi__qk8GH", "-LbK81kTRFgtDxWe1LhD",
                "-LbQA9hx_xM0Zn6NGDWR", "-LcI0vxYtieV94FV3QDa", "-LcI9NHoLxY6cX2OSw79"]

def getLocalIp():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(("8.8.8.8", 80))
    ip = s.getsockname()[0]
    s.close()
    return ip

def createUpdateFile():
    if(os.path.isfile('./conf.json')):
        f = open("conf.json", "r+")
        data = json.load(f)
        key = data["key"]
        data = {'key':key}
        data = parse.urlencode(data).encode()
        req = request.Request("https://luizalex.herokuapp.com/api/getcamera", data)
        response = request.urlopen(req).read() 
        response = json.loads(response)
        response["key"] = key
        del response["NPersons"]
        f.close()
        f = open("conf.json", "w+")
        json.dump(response, f, indent=4)
        f.close()
    else:
        data = {'setor':'DEFAULT', 'ip': getLocalIp(), 'operando':'1'}     
        data = parse.urlencode(data).encode()
        req = request.Request("https://luizalex.herokuapp.com/api/addcamera", data)
        response = request.urlopen(req).read(); 
        response = json.loads(response)
        
        key = response["Key"]
        data = {'key':key}
        data = parse.urlencode(data).encode()
        req = request.Request("https://luizalex.herokuapp.com/api/getcamera", data)
        response = request.urlopen(req).read()
        response = json.loads(response)
        response["key"] = key
        f = open("conf.json", "w+")
        json.dump(response, f, indent=4)
        f.close()

def getKey():
    f = open("conf.json", "r")
    data = json.load(f)
    key = data["key"]
    f.close()
    return key

def addUpdatePerson(key = "-LabvTn-w9QTNwV2uNQ5", number = random.randint(0, 20)):
    now = datetime.datetime.now()
    data = {
        'hour' : now.hour,
        'day' : now.date,
        'month' : now.month,
        'year' : now.year,
        'key' : key,
        'counter' : number
    }
    data = parse.urlencode(data).encode()
    req = request.Request("https://luizalex.herokuapp.com/api/addcounter", data)
    request.urlopen(req)

def main():
    createUpdateFile()

main()